#!/usr/bin/env python3
import argh
import json
import random
import requests
import shutil
import subprocess
import yaml


def load_unrepro_pkgs():
    with open('reproducible.json') as f:
        pkgs = json.load(f)

    print(f'[+] {len(pkgs)} packages loaded...')

    pkgs = list(filter(lambda x: x['status'] == 'BAD', pkgs))
    print(f'[+] {len(pkgs)} unreproducible packages...')

    return pkgs


def load_classifications():
    with open('packages.yml') as f:
        classifications = yaml.safe_load(f.read())
    print(f'[+] {len(classifications)} classifications loaded...')
    return classifications


def run():
    pkgs = load_unrepro_pkgs()
    classifications = load_classifications()

    random.shuffle(pkgs)

    for pkg in pkgs:
        if pkg['name'] in classifications:
            continue

        if not pkg['has_diffoscope']:
            continue

        url = f'https://reproducible.archlinux.org/api/v0/builds/{pkg["build_id"]}/diffoscope'
        print(f'\033[1m => {pkg["name"]}\033[0m (\033[92m{pkg["version"]}\033[0m) - {url}')
        subprocess.run(['xdg-open', url])

        #print(pkg)

        output = []

        def p(x):
            output.append(x)
            print(x)

        p(pkg['name'] + ':')
        p(f'  version: {pkg["version"]}')
        p('  issues:')

        if pkg['name'].startswith('haskell-'):
            has_notes = True
            p(f'  - "haskell"')
        else:
            has_notes = False
            while True:
                x = input('  - ')
                if x == 'quit':
                    return
                if x == '':
                    break
                has_notes = True
                p(f'  - "{x}"')

        if has_notes:
            with open('packages.yml', 'a') as f:
                for x in output:
                    print(x, file=f)


def stats():
    pkgs = load_unrepro_pkgs()
    classifications = load_classifications()

    classified = list(filter(lambda x: x['name'] in classifications, pkgs))
    percent = 100.0 / len(pkgs) * len(classified)
    yield f'[+] {len(classified)} ({percent:.2f}%) unreproducible packages are classified'


def update(url='https://reproducible.archlinux.org/api/v0/pkgs/list'):
    with requests.get(url, stream=True) as r:
        r.raw.decode_content = True
        with open('reproducible.json', 'wb') as f:
            shutil.copyfileobj(r.raw, f)


if __name__ == '__main__':
    argh.dispatch_commands([run, stats, update])
