#!/usr/bin/env python3
import json
import yaml
import os

with open('packages.yml') as f:
    pkgs = yaml.safe_load(f.read())

os.makedirs('public', exist_ok=True)

with open('public/packages.json', 'w') as f:
    json.dump(pkgs, f)
