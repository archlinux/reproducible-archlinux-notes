# reproducible-archlinux-notes

Classified packages can be found in `packages.yml`.

See also:

- [reproducible debian](https://salsa.debian.org/reproducible-builds/reproducible-notes)

## Usage

    pacman -S python-argh python-requests python-yaml

Update your copy of the current reproducible.archlinux.org results

    ./classify.py update

Show current stats about classified packages:

    % ./classify.py stats
    [+] 12720 packages loaded...
    [+] 1633 unreproducible packages...
    [+] 562 classifications loaded...
    [+] 886 (54.26%) unreproducible packages are classified

Run the interactive classifier

    % ./classify.py run  
    [+] 12720 packages loaded...
    [+] 1633 unreproducible packages...
    [+] 562 classifications loaded...
     => python-pydot (1.4.2-1) - https://reproducible.archlinux.org/api/v0/builds/140707/diffoscope
    python-pydot:
      version: 1.4.2-1
      issues:
      - 

A new tab automatically opens in your browser with the diffoscope diff for you to inspect. Either skip the package by pressing just enter, or take notes (confirm each note with enter) and end with an empty line after you're done. It automatically continues with the next unclassified package. Quit with ctrl+c.

Try avoiding special characters, especially double quotes.

## Contributing

After you've classified some packages, run this command:

    ./fmt.py

Then `git commit` and push your changes.

## License

Licensed under either of Apache License, Version 2.0 or MIT license at your option.
