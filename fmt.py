#!/usr/bin/env python3
import yaml

with open('packages.yml') as f:
    pkgs = yaml.safe_load(f.read())

print(f"[+] Loaded {len(pkgs)} packages")

# Sort the dictionary
pkgs = {k: pkgs[k] for k in sorted(pkgs.keys())}

with open('packages.yml', 'w') as f:
    f.write(yaml.safe_dump(pkgs, sort_keys=False))

print(f"[+] Wrote {len(pkgs)} packages")
